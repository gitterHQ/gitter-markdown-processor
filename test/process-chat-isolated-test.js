'use strict';

const assert = require('assert');
const fs = require('fs');
const path = require('path');
const Processor = require('..');

function listTestPairs() {
  const dir = path.join(__dirname, 'markdown-conversions');

  const items = fs.readdirSync(dir);
  return items
    .filter(file => /\.markdown$/.test(file))
    .map(file => {
      const markdownFile = path.join(dir, file);
      const name = file.replace('.markdown', '');
      const htmlFile = markdownFile.replace('.markdown', '.html');
      const markdown = fs.readFileSync(markdownFile, { encoding: 'utf8' });
      const expectedHtml = fs.readFileSync(htmlFile, { encoding: 'utf8' });

      return {
        name,
        markdownFile,
        htmlFile,
        markdown,
        expectedHtml,
      };
    });
}

describe('process-chat', () => {
  const processor = new Processor();

  after(callback => {
    processor.shutdown(() => {
      // Add an extra time on cos mocha will just exit without waiting
      // for the child to shutdown
      setTimeout(callback, 150);
    });
  });

  describe('tests', () => {
    listTestPairs().forEach(item => {
      it(`should handle ${item.name}`, done => {
        processor.process(item.markdown, (err, result) => {
          if (err) return done(err);

          const { html } = result;
          assert.equal(html.trim(), item.expectedHtml.trim());
          done();
        });
      });
    });
  });

  describe.skip('performance tests', () => {
    listTestPairs().forEach(item => {
      it(`should handle ${item.name}`, done => {
        let completed = 0;
        for (let i = 0; i < 1000; i++) {
          processor.process(item.markdown, (err, result) => {
            completed++;
            if (err) return done(err);

            const { html } = result;
            assert.equal(html.trim(), item.expectedHtml.trim());

            if (completed === 1000) return done();
          });
        }
      });
    });
  });
});
