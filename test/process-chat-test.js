'use strict';

const assert = require('assert');
const fs = require('fs');
const path = require('path');
const processChat = require('../lib/process-chat');

describe('process-chat', () => {
  const dir = path.join(__dirname, 'markdown-conversions');

  const items = fs.readdirSync(dir);
  items
    .filter(file => /\.markdown$/.test(file))
    .forEach(file => {
      const markdownFile = path.join(dir, file);
      const htmlFile = markdownFile.replace('.markdown', '.html');
      const markdown = fs.readFileSync(markdownFile, { encoding: 'utf8' });
      const expectedHtml = fs.readFileSync(htmlFile, { encoding: 'utf8' });
      it(`should handle ${file}`, () => {
        const { html } = processChat(markdown);
        assert.equal(html.trim(), expectedHtml.trim());
      });
    });

  it('should isolate link references between messages', () => {
    const inputMd1 = '[Community for developers to chat][1]\n\n[1]: https://gitter.im/';
    const inputMd2 = 'arr[1]';
    const html1 = processChat(inputMd1).html;
    assert.equal(
      html1.trim(),
      '<a href="https://gitter.im/" rel="nofollow noopener noreferrer" target="_blank" class="link ">Community for developers to chat</a>',
    );
    const html2 = processChat(inputMd2).html;
    assert.equal(html2.trim(), 'arr[1]');
  });

  it('proxies link when camo config available', () => {
    const htmlImage = processChat('![](http://evil.com/asdf.jpg)', {
      camoUrl: 'https://user-content.gitter-static.net',
      camoSecret: '123',
    }).html;

    assert(
      htmlImage.indexOf(
        'https://user-content.gitter-static.net/81f362737c0a437a6c34179a2284c564040d43ae/687474703a2f2f6576696c2e636f6d2f617364662e6a7067',
      ) !== -1,
      `Image ${htmlImage} should have been proxied`,
    );
  });

  describe('invalid and suspicious links', () => {
    it('replaces URL with javascript: and data:', () => {
      const examples = [
        '[click here](data:text/html;base64,PHNjcmlwdD5hbGVydCgiSGVsbG8iKTs8L3NjcmlwdD4=)',
        // eslint-disable-next-line no-script-url
        '[click here](javascript:alert(1))',
      ];
      const htmlLinks = examples.map(markdown => processChat(markdown).html);
      htmlLinks.forEach(link =>
        assert(
          link.indexOf('https://goo.gl/7NDM3x') !== -1,
          `should replace their link (${link}) with a rickroll video`,
        ),
      );
    });
    it('adds http to links without correct protocol', () => {
      const htmlLink = processChat('[label](www.example.com)').html;
      assert(
        htmlLink.indexOf('http://www.example.com') !== -1,
        `Link ${htmlLink} should have http:// prepended`,
      );
    });

    it('should add a tooltip class to links that are not normalized/valid', () => {
      const examples = [
        'http://example.com/evil\u202E3pm.exe',
        '[evilexe.mp3](http://example.com/evil\u202E3pm.exe)',
        'rdar://localhost.com/\u202E3pm.exe',
        'http://one😄two.com',
        '[Evil-Test](http://one😄two.com)',
        'http://\u0261itlab.com',
        '[Evil-GitLab-link](http://\u0261itlab.com)',
      ];
      const htmlLinks = examples.map(markdown => processChat(markdown).html);
      htmlLinks.forEach(link =>
        assert(link.indexOf('tooltip') !== -1, `Link ${link} is missing tooltip`),
      );
    });

    it('should use normalized href for IDN hosts', () => {
      const idnLink = 'http://one😄two.com';
      const link = processChat(idnLink).html;
      // Test the displayed URL text in the <a>xxx</a> tag
      assert(link.indexOf('>http://one😄two.com<') !== -1, `Link label got changed: ${link}`);
      // Test the href attribute
      assert(
        link.indexOf('href="http://xn--onetwo-yw74e.com/"') !== -1,
        `Link href should be in punycode: ${link}`,
      );
    });

    it('escapes RTLO characters', () => {
      // Rendered text looks like it does not have any extra RTLO characters in it - "http://example.com/evilexe.mp3"
      // Actual dodgy string with RTLO character in it - http://example.com/evil‮3pm.exe
      const evilLink = 'http://example.com/evil\u202E3pm.exe';
      const link = processChat(evilLink).html;
      assert(
        link.indexOf('http://example.com/evil%E2%80%AE3pm.exe') !== -1,
        `RTLO character is not escaped: ${link}`,
      );
      assert(
        link.indexOf('\u202E') === -1,
        `Doesn't leave any unescaped RTLO characters anywhere in the link ${link}`,
      );
    });

    it('should add no tooltips for safe links', () => {
      const goodExamples = [
        'http://example.com',
        '[Safe-Test](http://example.com)',
        'https://commons.wikimedia.org/wiki/File:اسكرام_2_-_تمنراست.jpg',
        '[Wikipedia-link](https://commons.wikimedia.org/wiki/File:اسكرام_2_-_تمنراست.jpg)',
      ];

      const htmlLinks = goodExamples.map(markdown => processChat(markdown).html);
      htmlLinks.forEach(link =>
        assert(link.indexOf('tooltip') === -1, `Link ${link} shouldn't have a tooltip`),
      );
    });
  });
});
