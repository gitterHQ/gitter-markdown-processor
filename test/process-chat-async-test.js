'use strict';

const assert = require('assert');
const fs = require('fs');
const path = require('path');
const processChatAsync = require('../lib/process-chat-async');

describe('process-chat-async', () => {
  const dir = path.join(__dirname, 'markdown-conversions');

  const items = fs.readdirSync(dir);
  items
    .filter(file => /\.markdown$/.test(file))
    .forEach(file => {
      const markdownFile = path.join(dir, file);
      const htmlFile = markdownFile.replace('.markdown', '.html');
      const markdown = fs.readFileSync(markdownFile, { encoding: 'utf8' });
      const expectedHtml = fs.readFileSync(htmlFile, { encoding: 'utf8' });

      it(`should handle ${file}`, () =>
        processChatAsync(markdown).then(result => {
          const { html } = result;
          assert.equal(html.trim(), expectedHtml.trim());
        }));
    });

  it('should detect japanese', () =>
    processChatAsync('世界こんにちは、お元気ですか？').then(result => {
      assert.equal(result.lang, 'ja');
    }));

  it('should detect korean', () =>
    processChatAsync('세계 안녕하세요, 어떻게 지내 ?').then(result => {
      assert.equal(result.lang, 'ko');
    }));

  it('should detect russian', () =>
    processChatAsync('Привет мир , как ты?').then(result => {
      assert.equal(result.lang, 'ru');
      return processChatAsync('1. Привет мир , как ты?');
    }));

  it('should detect chinese (simplified)', () =>
    processChatAsync('您好，欢迎来到小胶质').then(result => {
      assert.equal(result.lang, 'zh');
    }));

  it('should detect chinese (traditional)', () =>
    processChatAsync('您好，歡迎來到小膠質').then(result => {
      assert.equal(result.lang, 'zh-Hant');
    }));

  it('should detect afrikaans', () =>
    processChatAsync('hoe is jy meneer?')
      .then(result => {
        assert.equal(result.lang, 'af');
        return processChatAsync('## hoe is jy meneer?');
      })
      .then(result => {
        assert.equal(result.lang, 'af');
      }));

  it('should deal with unreliable text snippets', () =>
    processChatAsync('あ、app/assets/javascripts/main.js は requirejs.config なんですか').then(
      result => {
        assert.equal(result.lang, 'ja');
      },
    ));

  it('should handle greek', () =>
    processChatAsync(
      'Μουλιάζουμε τα ξερά σύκα στο κρασί. Ζεσταίνουμε σε τηγάνι τη 1 κουτ. σούπας λάδι και σοτάρουμε το μπέικον, μέχρι να ροδίσει. Αλατοπιπερώνουμε και ρίχνουμε το χυμό λεμονιού,το υπόλοιπο λάδι και το σπανάκι. Ανακατεύουμε ίσα να λαδωθεί το σπανάκι και να μαραθεί λίγο. Στραγγίζουμε τα σύκα και τα ανακατεύουμε με το μείγμα του τηγανιού. Απλώνουμε τη σαλάτα πάνω στις φρυγανισμένες φέτες ψωμί και σερβίρουμε',
    ).then(result => {
      assert.equal(result.lang, 'el');
    }));

  it('should handle null', () => processChatAsync(null));
});
