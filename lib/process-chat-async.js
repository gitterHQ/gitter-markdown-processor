'use strict';

const Promise = require('bluebird');
const processChat = require('./process-chat');
const detectLang = require('./detect-lang');

module.exports = exports = function processChatAsync(text, { camoUrl, camoSecret } = {}, callback) {
  return Promise.try(() => processChat(text, { camoUrl, camoSecret }))
    .then(result => {
      const plainText = result.plainText.trim();

      if (!plainText) return result;
      return detectLang(plainText).then(lang => {
        result.lang = lang;
        return result;
      });
    })
    .nodeify(callback);
};
